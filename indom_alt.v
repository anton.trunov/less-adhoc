(*
    Copyright (C) 2012  G. Gonthier, B. Ziliani, A. Nanevski, D. Dreyer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

From Coq Require Import ssreflect ssrfun ssrbool.
From mathcomp Require Import ssrnat seq eqtype.
Require Import heaps.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


(*****************************************************************)
(* indom :                                                       *)
(*    lemma automated with Canonical Structures to prove/rewrite *)
(*    expressions with the form                                  *)
(*      x \in dom (... :+ x :-> v :+ ... )                       *)
(*    for some v. Usage:                                         *)
(*      rewrite/apply: (indom D)                                 *)
(*    where D : def (... :+ x :-> v :+ ...)                      *)
(*****************************************************************)

Structure tagged_heap := Tag {untag :> heap}.

Canonical Structure singleton_tag A x (v : A) := Tag (x :-> v).
Canonical Structure left_tag h1 h2 := Tag (h1 :+ h2).

Definition invariant x (h : tagged_heap) :=
  def h -> x \in dom h.

Structure find x :=
  Find { heap_of :> tagged_heap;
         _ : invariant x heap_of }.

Arguments Find : clear implicits.


Lemma indom (x : ptr) (f : find x) :
        def f -> x \in dom f.
Proof. by case: f. Qed.

Lemma ptr_inv A x (v : A) : invariant x (Tag (x :-> v)).
Proof. by rewrite /invariant defPt domPt inE eqxx. Qed.

Canonical Structure ptr_found A x v :=
  Find x (singleton_tag x v) (@ptr_inv A x v).

Example test1 (x1 : ptr) :
          def (x1 :-> 1) -> x1 \in dom (x1 :-> 1).
Proof. by move=>D; rewrite indom. Qed.

Lemma left_inv x h (f : find x) : invariant x (Tag (f :+ h)).
Proof.
case:f=>[[i]]; rewrite /invariant /= => H D.
by rewrite domUn !inE /= D (H (defUnl D)).
Qed.

Canonical Structure search_left x h (f : find x) :=
  Find x (left_tag f h) (@left_inv x h f).

Example test2 (x1 x2 : ptr) :
          def (x1 :-> 1 :+ x2 :-> 2) -> x1 \in dom (x1 :-> 1 :+ x2 :-> 2).
Proof. by move=>D; rewrite indom. Qed.

Lemma right_inv x h (f : find x) : invariant x (Tag (h :+ f)).
Proof.
case: f=>[[i]]; rewrite /invariant /= => H D.
by rewrite domUn !inE /= D (H (defUnr D)) orbT.
Qed.

Canonical Structure search_right x h (f : find x) :=
  Find x (Tag (h :+ f)) (@right_inv x h f).

Example test3 (x1 x2 : ptr) :
          def (x1 :-> 1 :+ x2 :-> 2) -> x2 \in dom (x1 :-> 1 :+ x2 :-> 2).
Proof. by move=>D; rewrite indom. Qed.

(*************************************************)
(*                   Examples                    *)
(*************************************************)

(* simple example *)
Example ex1 A (x1 x2 : ptr) (v1 v2 : A) (h1 h2 : heap) :
          def (h1 :+ x1 :-> 1 :+ (x2 :-> 3 :+ empty)) -> 
          if x2 \in dom (h1 :+ x1 :-> 1 :+ (x2 :-> 3 :+ empty)) 
            then 1 == 1 
            else 1 == 0.
Proof.
move=>D.
by rewrite indom.
Qed.

(* same example, automatically unfolding a definition *)
Example ex2 A (x1 x2 : ptr) (v1 v2 : A) (h1 h2 : heap) :
          def (h1 :+ x1 :-> 1 :+ (x2 :-> 3 :+ empty)) -> 
          if x2 \in dom (h1 :+ x1 :-> 1 :+ (x2 :-> 3 :+ empty)) 
            then 1 == 1 
            else 1 == 0.
Proof.
set H := _ :+ _ :+ (_ :+ _).
move=>D.
by rewrite indom.
Qed.
